(* Welcome to the OCaml Starter for Ultimate TicTacToe on theaigames.com
 * Look at ulttt.ml for more information.
 *)
open Td;;
open Ulttt;;

let main gstate =
  debug "a";
  let moves = Ulttt.legal_moves gstate in
  debug "b";
  let move = Ulttt.random_from_list moves in
  debug "c";
    Ulttt.issue_order move;
  debug "d";
;;

Ulttt.run_bot main

