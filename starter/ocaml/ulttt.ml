(* Welcome to the OCaml Starter for Ultimate TicTacToe on theaigames.com
 * Have a look at the utility functions at the end of this file.
 *
 * Also note that you may want to extend the data structures in td.ml
 * or use these as input-only and create your own data structures to
 * represent the game state. 
 *
 * It may be worth considering collapsing the cell and macro_cell data
 * types into a single type, as this can save you some messy type coercions
 * later. It may have other short-term consequences, I haven't tested this
 * approach in a bot yet.
 *
 * The debug function below is helpful for writing to stderr or a file.
 * Just remove the comment marks and () from the function below if you want 
 * to use it.
 *
 * If you find any bugs, please notify the
 * developer here:
     https://github.com/smiley1983/ultimate_tictactoe_ocaml
 *)

open Td;;

let aigames = true;;

let get_time () = Unix.gettimeofday ();;

let square_size = 3;;

(* output section *)

let out_chan = stderr (* open_out "mybot_err.log" *);; (* Debugging output *)

let debug s = ()
(*
   output_string out_chan s; 
   flush out_chan
*)
;;

let finish_turn () =
   if not aigames then
      Printf.printf "go\n%!";
;;

let issue_order (row, col) =
   let s = Printf.sprintf "place_move %d %d\n" col row in
   debug s;
   output_string stdout s;
   flush stdout;
   finish_turn();
;;

let text_cell = function
| `Neutral -> "+ "
| `Player1 -> "o "
| `Player2 -> "x "
;;

let text_board board =
  Array.iter (fun row ->
    debug "\n";
    Array.iter (fun cell ->
      debug (text_cell cell)
    ) row
  ) board;
  debug "\n\n"
;;

let text_macrocell = function
| `Neutral -> ". "
| `Player1 -> "o "
| `Player2 -> "x "
| `Active -> "! "
| `Draw -> "- "
;;

let text_macroboard macroboard =
  debug "text_macroboard\n";
  Array.iter (fun row ->
    debug "\n";
    Array.iter (fun cell ->
      debug (text_macrocell cell)
    ) row
  ) macroboard;
  debug "\n\n"
;;

(* End output section *)

(* input processing *)

let owner gstate i =
  if (i = gstate.setup.your_botid) then `Mine
  else `Theirs
;;

let int_to_cell gstate i =
  match i with
  | 0 -> `Neutral
  | n -> owner gstate i
;;

let int_to_square gstate i =
  match i with
  | (-1) -> `Active
  | 0 -> `Neutral
  | n -> owner gstate i
;;

let uncomment s =
  try String.sub s 0 (String.index s '#')
  with Not_found -> s
;;

let new_board () =
   let size = square_size * square_size in
   Array.make_matrix size size `Neutral
;;

let new_macroboard () =
   let size = square_size in
   Array.make_matrix size size `Active
;;

let clear_gstate gstate =
 (
  gstate.board <- new_board ();
  gstate.macroboard <- new_macroboard ();
  gstate.round <- 0;
  gstate.move <- 0;
 )
;;

(* tokenizer from rosetta code *)
let split_char sep str =
  let string_index_from i =
    try Some (String.index_from str i sep)
    with Not_found -> None
  in
  let rec aux i acc = match string_index_from i with
    | Some i' ->
        let w = String.sub str i (i' - i) in
        aux (succ i') (w::acc)
    | None ->
        let w = String.sub str i (String.length str - i) in
        List.rev (w::acc)
  in
  aux 0 []

let update_game_board (gstate:game_state) data =
   (* Assume that this coincides approximately with start of turn *)
   gstate.last_update <- get_time();
   let size = square_size * square_size in
   List.iteri (fun i (c:string) ->
      let row = i / size in
      let col = i mod size in
         gstate.board.(row).(col) <- int_to_cell gstate (int_of_string c)
   ) (split_char ',' data) (* (Str.split (Str.regexp ",") data) *)
;;

let update_game_macroboard (gstate:game_state) data =
   List.iteri (fun i (c:string) ->
      let row = i / square_size in
      let col = i mod square_size in
         gstate.macroboard.(row).(col) <- int_to_square gstate (int_of_string c)
   ) (split_char ',' data ) 
;;

let update_timebank gstate v =
  gstate.last_timebank <- (int_of_string v);
;;

let action_move bot gstate =
  debug "action_move called\n";
  bot gstate;
  debug "action_move finished\n";
;;

let four_token (gstate:game_state) key t1 t2 t3 =
   if (t3 = "") || (t2 = "") || (t1 = "") || (key = "")  then (
     debug ("four_token fail: " ^ key ^ " " ^ t1 ^ " " ^ t2 ^ " " ^ t3 ^ "\n")
   ) else (
   match key with
    | "update" ->
      begin match t1 with
       | "game" ->
         begin match t2 with
          | "round" -> gstate.round <- int_of_string t3
          | "move" -> gstate.move <- int_of_string t3
          | "field" -> update_game_board gstate t3
          | "macroboard" -> update_game_macroboard gstate t3
          | _ -> ()
         end
       | _ -> ()
      end
    | _ -> ()
    )
;;

let three_token bot gstate key t1 t2 =
   if (t2 = "") || (t1 = "") || (key = "")  then ( 
     debug ("three_token fail: " ^ key ^ " " ^ t1 ^ " " ^ t2 ^ " " ^ "\n")
   ) else (
     match key with
      | "settings" -> 
        begin match t1 with
         | "timebank" -> gstate.setup.timebank <- int_of_string t2
         | "time_per_move" -> gstate.setup.time_per_move <- int_of_string t2
         | "player_names" -> gstate.setup.player_names <- split_char ',' t2 
         | "your_bot" -> gstate.setup.your_bot <- t2
         | "your_botid" -> gstate.setup.your_botid <- int_of_string t2
         | _ -> ()
        end
      | "action" -> 
        begin match t1 with
         | "move" -> update_timebank gstate t2; 
	   if aigames then action_move bot gstate 
         | _ -> ()
        end
      | _ -> ()
   )
;;

let one_token bot gstate token =
  match token with
  | "ready" -> finish_turn()
  | "go" -> action_move bot gstate
  | _ -> ()
;;

let process_line bot gstate line =
  debug (line ^ "\n");
  let tokens = split_char ' ' (uncomment line) in
  match List.length tokens with
  | 4 -> four_token gstate (List.nth tokens 0) (List.nth tokens 1) (List.nth tokens 2) (List.nth tokens 3)
  | 3 -> three_token bot gstate (List.nth tokens 0) (List.nth tokens 1) (List.nth tokens 2)
  | 1 -> one_token bot gstate (List.nth tokens 0)
  | _ -> debug ("Incorrect bot input: " ^ line ^ "\n")
;;

let read_lines bot gstate =
  while true do
    let line = read_line () in
      process_line bot gstate line;
  done
;;

(* End input section *)

(* Utility functions *)

let random_from_list lst =
  let len = List.length lst in
    List.nth lst (Random.int len)
;;

let is_legal gstate row col =
  let mbr = row / square_size in
  let mbc = col / square_size in
    (gstate.macroboard.(mbr).(mbc) = `Active) 
    && (gstate.board.(row).(col) = `Neutral)
;;

let legal_moves gstate =
  let result = ref [] in
  let size = (square_size * square_size) - 1 in
  for row = 0 to size do
    begin for col = 0 to size do
      if is_legal gstate row col then result := (row, col) :: !result;
    done end
  done;
  !result
;;

let time_elapsed_this_turn gstate =
  (get_time() -. gstate.last_update) *. 1000.
;;

let time_remaining gstate =
  (float_of_int gstate.last_timebank -. time_elapsed_this_turn gstate)
;;

(* End utility *)

let run_bot bot =
  (* Remove or change the self_init if you want predictable random sequences *)
  Random.self_init (); 
  let game_info =
     {
      timebank = 0;
      time_per_move = 0;
      player_names = [];
      your_bot = "";
      your_botid = -1;
     }
  in
  let game_state =
     {
      board = [|[| |]|];
      macroboard = [|[| |]|];
      round = 0;
      move = 0;
      setup = game_info;
      last_update = 0.0;
      last_timebank = 0;
     }
  in
  clear_gstate game_state;

  begin try
   (
     read_lines bot game_state
   )
  with exc ->
   (
    debug (Printf.sprintf
       "Exception in turn %d :\n" game_state.round);
    debug (Printexc.to_string exc);
    raise exc
   )
  end;
;;


